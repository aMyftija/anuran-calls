import pandas as pd

def edit_df(df: pd.DataFrame, classifier:str, using_smote=False, inplace=True)-> pd.DataFrame :
    '''
    gives a more compact view of the GridSerachCV results
    '''
    
    df.drop(columns=["mean_score_time", "std_score_time", "mean_fit_time", "std_fit_time"], inplace=True)
    df.columns = df.columns.str.replace("param_", "")

    df.rename(columns={
        "pca__n_components": "n_pc",
        "rank_test_score": "rank"
        },
        inplace=True)
    df.columns = df.columns.str.replace("test_score", "score")
    
    df.rename(columns={
        "split0_score": "score_0",
        "split1_score": "score_1",
        "split2_score": "score_2",
        "split3_score": "score_3",
        "split4_score": "score_4",
        "split5_score": "score_5",
        "split6_score": "score_6",
        "split7_score": "score_7",
        "mean_score": "score_m",
        "std_score": "score_s"
    }, inplace=True)

    if "split0_train_score" in list(df.columns):
        df.drop(columns=["split0_train_score",
                        "split1_train_score",
                         "split2_train_score",
                         "split3_train_score",
                         "split4_train_score",
                         "split5_train_score",
                         "split6_train_score",
                         "split7_train_score"],
                inplace=True)


    if classifier=="dt" or classifier=="tree":
        df.rename(columns={
            "dt__class_weight": "c_weight",
            "dt__criterion": "tree:crit",
            "dt__ccpalpha": "tree:ccpalpha",
            },
            inplace=True)

    if classifier=="rf":
        df.rename(columns={
            "rf__class_weight": "c_weight",
            "rf__max_features": "rf:max_f",
            "rf__n_estimators": "rf:n_trees",
            "rf__criterion": "rf:crit"
            },
            inplace=True)
        
    if classifier=="svc":
        df.rename(columns={
            "svc__class_weight": "c_weight",
            "svc__C": "svm:C",
            "svc__gamma": "svc:gamma",
            "svc__kernel": "svc:kernel"
            },
            inplace=True)
        
    
    if using_smote:
        df.rename(columns={"smote__sampling_strategy": "oversampling"}, inplace=True)
        
    
    df.drop(columns="params", inplace=True)
    df["score_*"] = df["score_m"] - df["score_s"]
    df.sort_values(by="score_*", ascending=False, inplace=True)
    
    return df
