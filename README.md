# Anuran Calls
This is a project delivered for the "Mathematics in Machine Learning" course of the "Data Science & Engineering" master's degree @ Politecnico di Torino on 2022-September.

## About
The purpose of the project analyse different machine-learning algorithms from a mathematical and practical point of view. 

It deals with multi-label classification of the [UCI Anuran Calls dataset](https://archive.ics.uci.edu/ml/datasets/Anuran+Calls+%28MFCCs%29). The goal is to properly classify data frogs' families starting from their MFCCs (Mel Spectrum Cepstral Coefficients).
